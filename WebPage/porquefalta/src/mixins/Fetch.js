const baseURL = 'https://localhost:5001/api/'

async function request (route, params, method = 'GET', responseType = 'json') {
  const options =
  {
    method,
    headers: {
      'ContentType': `application/${responseType}`,
      'Authorization': 'Bearer ' + localStorage.getItem('token')
    }
  }

  if (params) {
    if (method === 'GET') {
      route += `/${objectToQueryString(params)}`
    } else {
      options.body = params
    }
  }
  if (responseType === 'blob') delete options.headers.ContentType

  let response = null

  try {
    response = await fetch(`${baseURL}${route}`, options)
  } catch (e) {
    return generateErrorResponse(`Ocorreu um erro ao realizar a requisição. - ${e.message}`, response.status)
  }

  if (response.status !== 200) {
    if (responseType === 'blob') {
      const result = await response[responseType]()
      return result
    } else {
      return generateErrorResponse(`Erro no envio de arquivos.`, response.status)
    }
  }

  const result = await response[responseType]()

  return result
}

function objectToQueryString (obj) {
  if (typeof obj !== 'object') {
    return `${obj}`
  }
  return obj.join('/').slice(0, -1)
}

function generateErrorResponse (message, code) {
  return {
    status: 'error',
    message,
    code
  }
}

function get (route, params, responseType) {
  return request(route, params, 'GET', responseType)
}

function post (route, params) {
  return request(route, params, 'POST')
}

function update (route, params) {
  return request(route, params, 'PUT')
}

function remove (route, params) {
  return request(route, params, 'DELETE')
}

export default {
  get,
  post,
  update,
  remove
}
