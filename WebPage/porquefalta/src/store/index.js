import Vue from 'vue'
import Vuex from 'vuex'

import Global from './Global'
import Login from '@/components/Login/store'
import Home from '@/components/Home/store'

Vue.use(Vuex)

const modules = {
  Global,
  Login,
  Home
}


export default new Vuex.Store({
  modules
})
