const isLoggedIn = (state) => {
  return !!state.userAuth.token
}

const tokenCheck = (state) => {
  return state.tokeCheck
}

export default {
  isLoggedIn,
  tokenCheck
}