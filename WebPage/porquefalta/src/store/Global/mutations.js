const SETUSERAUTH = (state, fields) => {
  state.userAuth = {
    token: localStorage.getItem('token') || '',
    email: fields.email,
    nome: fields.nome,
    id: fields.id
  }
}

const LOGOUT = (state) => {
  state.userAuth = {
    token: '',
    email: '',
    nome: '',
    id: '',
  }
}

const SETGENERICOK = (state) => {
  state.ok = true
}

const TOKENCHECK = (state, val) => {
  state.tokenCheck = val
}

export default {
  SETUSERAUTH,
  LOGOUT,
  SETGENERICOK,
  TOKENCHECK
}