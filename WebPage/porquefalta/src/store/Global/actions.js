import Fetch from '../../mixins/Fetch'

const logout = ({ commit }) => {
  localStorage.removeItem('token')
  localStorage.removeItem('id')
  localStorage.removeItem('nome')
  localStorage.removeItem('email')
  localStorage.removeItem('descPerfil')
  localStorage.removeItem('idPerfil')
  commit('LOGOUT')
}

const checkToken = async ({ commit }) => {
  const request = await Fetch.get('check/checktoken')

  if(request.status === 'error') throw new Error("Falha de autenticação.")

  commit('Global/TOKENCHECK', request, { root: true })
}

export default {
  logout,
  checkToken
}