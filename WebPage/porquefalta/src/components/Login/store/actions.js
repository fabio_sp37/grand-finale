import Fetch from '../../../mixins/Fetch'

const autenticar = async ({ commit }, objAuth) => {
  const request = await Fetch.post('user/auth', objAuth)

  if(request.status === 'error') throw new Error(request.message)

  localStorage.setItem('token', request.token)
  localStorage.setItem('email', request.email)
  localStorage.setItem('nome', request.nome)
  localStorage.setItem('id', request.id)
  localStorage.setItem('descPerfil', request.descPerfil)
  localStorage.setItem('idPerfil', request.idPerfil)

  commit('Global/SETUSERAUTH', request, { root: true })

  return request
}


const cadastro = async ({ commit }, objCad) => {
  const request = await Fetch.post('user/add', objCad)
  if(request.status === 'error') throw new Error(request.message)

  commit('GLOBAL/SETGENERICOK')

  return request
}

export default {
  autenticar,
  cadastro
}