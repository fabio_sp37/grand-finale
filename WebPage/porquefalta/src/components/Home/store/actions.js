import Fetch from '../../../mixins/Fetch'

const cadFaltaAgua = async ({ commit }, objForm) => {
  const request = await Fetch.post('faltaagua/add', objForm)

  if(request.status === 'error') throw new Error(request.message)

  commit('Global/SETGENERICOK', '', { root: true })

  return request
}

export default {
  cadFaltaAgua
}