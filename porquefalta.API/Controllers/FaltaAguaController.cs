﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using porquefalta.MO;

namespace porquefalta.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaltaAguaController : ControllerBase
    {
        private readonly BSS.FaltaAgua _faltaAgua;

        public FaltaAguaController(ILogger<RegisterLog> logger)
        {
            ILogger<RegisterLog> _logger = logger;
            _faltaAgua = new BSS.FaltaAgua(_logger);
        }

        /// <summary>
        /// Inclui nova solicitação de falta de água
        /// </summary>
        /// <param name="newEntity"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("add")]
        public async Task<JsonResult> Add([FromForm] MO.Request.RequestAddFaltaAgua newEntity)
        {
            _ = newEntity ?? throw new ArgumentNullException(nameof(newEntity));
            MessageResult result = await _faltaAgua.Include(newEntity).ConfigureAwait(false);
            JsonResult jsonResult = new JsonResult(result.Message)
            {
                ContentType = "application/json",
                StatusCode = result.Code
            };

            return jsonResult;
        }
    }
}
