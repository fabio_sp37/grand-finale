using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using porquefalta.BSS;
using porquefalta.MO;

namespace porquefalta.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
      private readonly BSS.User _user;

        public UserController(ILogger<RegisterLog> logger)
        {
            ILogger<RegisterLog> _logger = logger;
            _user = new BSS.User(_logger);
        }

        /// <summary>
        /// Efetua autenticação do usuário
        /// </summary>
        /// <param name="subscriptionSettings">Configuração de autenticação</param>
        /// <param name="reqUser">Dados de autenticação</param>
        /// <returns>Retorna o token do usuário autenticado</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("auth")]
        public async Task<JsonResult> Auth([FromServices] SubscriptionSettings subscriptionSettings, [FromForm] MO.Request.RequestAuthUser reqUser)
        {
            _ = reqUser ?? throw new ArgumentNullException(nameof(reqUser));
            MessageResult result = await _user.Auth(subscriptionSettings, reqUser).ConfigureAwait(false);

            JsonResult jsonResult = new JsonResult(result.Message)
            {
                ContentType = "application/json",
                StatusCode = result.Code
            };

            return jsonResult;
        }

        /// <summary>
        /// Adiciona novo usuário
        /// </summary>
        /// <param name="newEntity"></param>
        [HttpPost]
        //[Authorize("Bearer")]
        [Route("add")]
        public async Task<JsonResult> Add([FromForm] MO.Request.RequestAddUser newEntity)
        {
            _ = newEntity ?? throw new ArgumentNullException(nameof(newEntity));
            MessageResult result = await _user.Include(newEntity).ConfigureAwait(false);
            JsonResult jsonResult = new JsonResult(result.Message)
            {
                ContentType = "application/json",
                StatusCode = result.Code
            };

            return jsonResult;
        }

        /// <summary>
        /// Deleta um usuário
        /// </summary>
        /// <param name="id">Identificação do usuário</param>
        [HttpGet]
        [Authorize("Bearer")]
        [Route("del/{id}")]
        public async Task<JsonResult> Del(Guid id)
        {
            _ = (id == Guid.Empty) ? throw new ArgumentNullException(nameof(id)) : id;
            MessageResult result = await _user.Delete(id).ConfigureAwait(false);
            JsonResult jsonResult = new JsonResult(result.Message)
            {
                ContentType = "application/json",
                StatusCode = result.Code
            };

            return jsonResult;
        }

        /// <summary>
        /// Atualiza o usuario
        /// </summary>
        /// <param name="newEntity">Novos dados do usuário</param>
        [HttpPost]
        [Authorize("Bearer")]
        [Route("up")]
        public async Task<JsonResult> Up([FromForm] MO.Request.RequestAddUser newEntity)
        {
            _ = newEntity ?? throw new ArgumentNullException(nameof(newEntity));
            MessageResult result = await _user.Update(newEntity).ConfigureAwait(false);
            JsonResult jsonResult = new JsonResult(result.Message)
            {
                ContentType = "application/json",
                StatusCode = result.Code
            };
            return jsonResult;
        }
    }
}