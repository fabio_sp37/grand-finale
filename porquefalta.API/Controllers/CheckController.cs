﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace porquefalta.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckController : ControllerBase
    {
        [HttpGet]
        [Route("check")]
        public IActionResult Check() => Ok(200);

        [HttpGet]
        [Authorize("Bearer")]
        [Route("checkToken")]
        public IActionResult CheckToken() => Ok(200);
    }
}
