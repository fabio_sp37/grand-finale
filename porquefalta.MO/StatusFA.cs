﻿using System.ComponentModel.DataAnnotations.Schema;

namespace porquefalta.MO
{
    [Table("TBSTATUSFA")]
    public class StatusFA : Global
    {
        [Column("DESCRICAO")]
        public string Descricao { get; set; }
    }
}
