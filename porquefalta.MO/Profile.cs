using System.ComponentModel.DataAnnotations.Schema;

namespace porquefalta.MO
{
    [Table("TBPROFILE")]
    public class Profile : Global
    {
        [Column("DESCRICAO")]
        public string Descricao { get; set; }
    }
}