using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace porquefalta.MO
{
    public class Global
    {
        [Key]
        [Column("ID")]
        public Guid Id { get; set; }
    }
}