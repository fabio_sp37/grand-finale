﻿namespace porquefalta.MO
{
    public class MessageResult
    {
        public object Message { get; set; }
        public int Code { get; set; }
    }

    public class MessageResult<T> where T : new()
    {
        public T Message { get; set; }
        public int Code { get; set; }
    }
}