using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace porquefalta.MO
{
    [Table("TBLOGEVENTS")]
    public class LogEvents : Global
    {
        [Column("IDUSER")]
        public Guid Usuario { get; set; }
        [Column("ACAO")]
        public string Acao { get; set; }
        [Column("DATAHORA")]
        public DateTime DataHora { get; set; } = DateTime.Now;
    }
}