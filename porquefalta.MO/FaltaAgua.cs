﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace porquefalta.MO
{
    [Table("TBFALTAAGUA")]
    public class FaltaAgua : Global
    {
        [Column("CEP")]
        public string Cep { get; set; }
        [Column("RUA")]
        public string Rua { get; set; }
        [Column("NUMERO")]
        public int Numero { get; set; }
        [Column("BAIRRO")]
        public string Bairro { get; set; }
        [Column("CIDADE")]
        public string Cidade { get; set; }
        [Column("REREFENCIA")]
        public string Referencia { get; set; }
        [Column("DESCRICAO")]
        public string Descricao { get; set; }
        [Column("IDSTATUS")]
        public Guid IdStatus { get; set; }
        [Column("IDUSER")]
        public Guid IdUser { get; set; }
    }
}
