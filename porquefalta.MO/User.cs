using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace porquefalta.MO
{
    [Table("TBUSER")]
    public class User : Global
    {
        [Column("NOME")]
        public string Nome { get; set; }
        [Column("EMAIL")]
        public string Email { get; set; }
        [Column("SENHA")]
        public string Senha { get; set; }
        [Column("IDPERFIL")]
        public Guid IdPerfil { get; set; }
        [Column("ATIVO")]
        public bool Ativo { get; set; }
    }
}