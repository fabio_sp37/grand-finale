using System;

namespace porquefalta.MO.Request
{
  public class RequestAddUser
  {
    public Guid Id { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public string Senha { get; set; }
    public Guid IdPerfil { get; set; }
    public bool Ativo { get; set; }
  }
}