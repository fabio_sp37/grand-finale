﻿using System;

namespace porquefalta.MO.Request
{
    public class RequestAddFaltaAgua
    {
        public string Cep { get; set; }
        public string Rua { get; set; }
        public int Numero { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Referencia { get; set; }
        public string Descricao { get; set; }
        public Guid IdUser { get; set; }
    }
}
