using System;

namespace porquefalta.MO
{
    public class PermissionPag
    {
        public Guid ID { get; set; }
        public Guid IdPerfil { get; set; }
        public string Rota { get; set; }
        public string NmRota { get; set; }
    }
}