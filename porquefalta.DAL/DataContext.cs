using Microsoft.EntityFrameworkCore;
using porquefalta.MO;
using Microsoft.Extensions.Configuration;
using System.IO;
using System;

namespace porquefalta.DAL
{
  public class DataContext : DbContext
  {
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      if (!optionsBuilder.IsConfigured)
      {
        IConfiguration builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

        base.OnConfiguring(optionsBuilder.UseOracle(builder["ConnectionStrings:PorQueFalta"]));
      }
    }
    public DbSet<User> Usuarios { get; set; }
    public DbSet<LogEvents> LogEventos { get; set; }
    public DbSet<Profile> Perfil { get; set; }
    public DbSet<FaltaAgua> FaltaAgua { get; set; }
    public DbSet<StatusFA> StatusFA { get; set; }
  }
}