using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;

namespace porquefalta.BSS
{
    public class SubscriptionSettings
    {
        public SecurityKey Key { get; set; }
        public SigningCredentials CredentialSignature { get; private set; }

        public SubscriptionSettings()
        {
            using (RSACryptoServiceProvider provider = new RSACryptoServiceProvider(2048))
                Key = new RsaSecurityKey(provider.ExportParameters(true));

            CredentialSignature = new SigningCredentials(Key, SecurityAlgorithms.RsaSha256Signature);
        }
    }
}