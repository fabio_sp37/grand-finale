﻿using Microsoft.Extensions.Logging;
using porquefalta.DAL;
using porquefalta.MO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace porquefalta.BSS
{
    public class FaltaAgua
    {
        private readonly MessageResult _message;
        private readonly ILogger<RegisterLog> _logger;

        public FaltaAgua(ILogger<RegisterLog> logger)
        {
            _message = new MessageResult();
            _logger = logger;
        }

        public async Task<MessageResult> Include(MO.Request.RequestAddFaltaAgua newEntity)
        {
            try
            {
                _logger.LogInformation(
                    $"START => { Utils.GetActualAsyncMethodName() } - PARAMS: { newEntity.Rua }, { newEntity.Cidade }"
                );

                using DataContext _dbContext = new DataContext();

                var status = _dbContext.StatusFA.Where(s => s.Descricao == "Pendente").Select(s => s.Id).FirstOrDefault();

                MO.FaltaAgua newFaltaAgua = new MO.FaltaAgua
                {
                    Cep = newEntity.Cep,
                    Rua = newEntity.Rua,
                    Bairro = newEntity.Bairro,
                    Cidade = newEntity.Cidade,
                    Numero = newEntity.Numero,
                    Referencia = newEntity.Referencia,
                    Descricao = newEntity.Descricao,
                    IdStatus = status,
                    IdUser = newEntity.IdUser
                };

                _dbContext.FaltaAgua.Add(newFaltaAgua);

                await _dbContext.SaveChangesAsync();

                _message.Message = new { newFaltaAgua = newFaltaAgua.Id };
                _message.Code = 200;

                _logger.LogInformation("END => successful");
                return _message;
            }
            catch (Exception ex)
            {
                _message.Message = "Falha ao incluir falta de agua";
                _message.Code = 400;

                _logger.LogError(ex, ex.Message);

                return _message;
            }
        }

    }
}
