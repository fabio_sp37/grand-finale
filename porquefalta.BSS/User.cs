﻿using porquefalta.DAL;
using porquefalta.MO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace porquefalta.BSS
{
    public class User
    {
        private readonly MessageResult _message;
        private readonly ILogger<RegisterLog> _logger;

        public User(ILogger<RegisterLog> logger)
        {
            _message = new MessageResult();
            _logger = logger;
        }

        /// <summary>
        /// Autenticação do usuário
        /// </summary>
        /// <param name="reqUser">Dados de requisição do usuário</param>
        /// <returns>Se a autenticação foi feita com sucesso retorna um token JWT.</returns>
        public async Task<MessageResult> Auth(SubscriptionSettings subscriptionSettings, MO.Request.RequestAuthUser reqUser)
        {
            try
            {
                _logger.LogInformation(
                    $"START => { Utils.GetActualAsyncMethodName() } => { typeof(BSS.User).FullName } - Params: Login { reqUser.Login }"
                );

                dynamic autenticado = new ExpandoObject();
                reqUser.Senha = Utils.Encrypt(reqUser.Senha);

                using (DataContext _dbContext = new DataContext())
                {
                    MO.User user = await (from u in _dbContext.Usuarios
                                              where u.Ativo
                                                  && u.Email == reqUser.Login
                                                  && u.Senha == reqUser.Senha
                                              select u).FirstOrDefaultAsync();

                    if (user is null)
                    {
                        _message.Code = 400;
                        _message.Message = "Usuário não encontrado";
                        _logger.LogInformation($"END => {_message.Message}");

                        return _message;
                    }

                    var descP = await (from u in _dbContext.Usuarios
                                       where u.Ativo
                                           && u.Email == reqUser.Login
                                           && u.Senha == reqUser.Senha
                                       join profile in _dbContext.Perfil on u.IdPerfil equals profile.Id
                                       select profile.Descricao).FirstOrDefaultAsync();

                    autenticado.token = Utils.GenerateToken(user, subscriptionSettings.CredentialSignature);
                    autenticado.email = user.Email;
                    autenticado.idPerfil = user.IdPerfil;
                    autenticado.nome = user.Nome;
                    autenticado.id = user.Id;
                    autenticado.descPerfil = descP;
                }

                _message.Message = autenticado;
                _message.Code = 200;

                Utils.SaveLogEvent(
                    new LogEvents()
                    {
                        Usuario = autenticado.id,
                        Acao = "Auth"
                    }
                );

                _logger.LogInformation($"END => { typeof(BSS.User).FullName } - successful");
                return _message;
            }
            catch (Exception ex)
            {
                _message.Message = "Falha de autenticação";
                _message.Code = 400;

                _logger.LogError(
                    ex,
                    ex.Message
                );

                return _message;
            }
        }
        
        /// <summary>
        /// Adiciona novo usuário
        /// </summary>
        /// <param name="User">Modelo de usuário a ser inserido</param>
        /// <returns>Retorna o objeto Guid do modelo inserido</returns>
        public async Task<MessageResult> Include(MO.Request.RequestAddUser newEntity)
        {
            try
            {
                _logger.LogInformation(
                    $"START => { Utils.GetActualAsyncMethodName() } - PARAMS: { newEntity.Email }, { newEntity.Nome }"
                );

                newEntity.Senha = Utils.Encrypt(newEntity.Senha);
                newEntity.Ativo = true;
                Profile p = new Profile();
                var perfis = await p.GetProfiles();
                using DataContext _dbContext = new DataContext();
                MO.User newUser = new MO.User
                {
                    Email = newEntity.Email,
                    IdPerfil = newEntity.IdPerfil == Guid.Empty ? perfis.Where(c => c.Descricao == "Default").Select(c => c.Id).FirstOrDefault() : newEntity.IdPerfil,
                    Nome = newEntity.Nome,
                    Senha = newEntity.Senha,
                    Ativo = newEntity.Ativo
                };

                _dbContext.Usuarios.Add(newUser);

                await _dbContext.SaveChangesAsync();

                _message.Message = new { newUser = newUser.Id };
                _message.Code = 200;

                _logger.LogInformation("END => successful");
                return _message;
            }
            catch (Exception ex)
            {
                _message.Message = "Falha ao incluir usuário";
                _message.Code = 400;

                _logger.LogError(ex, ex.Message);

                return _message;
            }
        }

        /// <summary>
        /// Atualiza o registro de um usuário
        /// </summary>
        /// <param name="id">Identificação do usuário</param>
        /// <param name="User">Parametros para atualização do usuário</param>
        /// <returns>Retorna 1 para sucesso e 0 para erro</returns>
        public async Task<MessageResult> Update(MO.Request.RequestAddUser newEntity)
        {
            try
            {
                _logger.LogInformation(
                    $"START => { Utils.GetActualAsyncMethodName() } - PARAMS: { newEntity.Email }, { newEntity.Nome }"
                );

                dynamic success = new { success = 0 };
                MO.User oldUser = new MO.User();

                using (DataContext _dbContext = new DataContext())
                {
                    oldUser = _dbContext.Usuarios.FirstOrDefault(i => i.Id == newEntity.Id);

                    if (oldUser != null)
                    {
                        oldUser.Nome = newEntity.Nome;
                        oldUser.Email = newEntity.Email;
                        oldUser.Senha = (newEntity.Senha == "**************************") ? oldUser.Senha : Utils.Encrypt(newEntity.Senha);
                        oldUser.IdPerfil = newEntity.IdPerfil;
                        oldUser.Ativo = newEntity.Ativo;

                        _dbContext.Usuarios.Update(oldUser);
                        await _dbContext.SaveChangesAsync();
                    }
                    else
                    {
                        success = new { success = 1 };
                    }
                }

                _message.Message = success;
                _message.Code = 200;

                _logger.LogInformation("END => successful");
                return _message;
            }
            catch (Exception ex)
            {
                _message.Message = "Falha ao atualizar usuário";
                _message.Code = 400;

                _logger.LogError(
                    ex,
                    ex.Message
                );

                return _message;
            }
        }

        /// <summary>
        /// Remove um usuário
        /// </summary>
        /// <param name="id">Identificação do usuário</param>
        /// <returns>Retorna 1 para sucesso e 0 para erro</returns>
        public async Task<MessageResult> Delete(Guid id)
        {
            try
            {
                _logger.LogInformation(
                    $"START => { Utils.GetActualAsyncMethodName() } - PARAMS: { id }"
                );

                MO.User User;
                using DataContext _dbContext = new DataContext();
                User = _dbContext.Usuarios.FirstOrDefault(i => i.Id == id);

                if (User != null)
                {
                    _dbContext.Remove(User);
                    await _dbContext.SaveChangesAsync();

                    _message.Message = 0;
                    _message.Code = 200;

                    return _message;
                }

                _message.Message = 0;
                _message.Code = 400;

                _logger.LogInformation("END => successful");
                return _message;
            }
            catch (Exception ex)
            {
                _message.Message = "Falha ao deletar usuário";
                _message.Code = 400;

                _logger.LogError(
                    ex,
                    ex.Message
                );

                return _message;
            }
        }

    }
}
