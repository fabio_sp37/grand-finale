using porquefalta.DAL;
using porquefalta.MO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;

namespace porquefalta.BSS
{
  public static class Utils
  {
    private const string key = @"g$C&^7GW:7pgY;BE*][qq.kT`v3dA,HBG<DTL#MpJ]B<JWGM2p=$[uezs8hA>Z\6VB7^'XG*%NDy{L$U^<";

    private static IConfigurationRoot LoadConfiguration()
    {
      IConfigurationBuilder builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json");

      IConfigurationRoot configuration = builder.Build();

      return configuration;
    }

    /// <summary>
    /// Gera token do usuário
    /// </summary>
    /// <param name="user">Usuário</param>
    /// <param name="assinatura">Assinatura de criptografia</param>
    /// <returns>Token</returns>
    public static string GenerateToken(MO.User user, SigningCredentials assinatura)
    {
      JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
      ClaimsIdentity identity = new ClaimsIdentity
      (
         new GenericIdentity(user.Id.ToString()),
         new[] {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.NameId, user.Nome),
                    new Claim(ClaimTypes.Role, user.IdPerfil.ToString()),
                    new Claim(ClaimTypes.Email, user.Email)
          }
      );

      SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
      {
        Issuer = "PorQueFaltaIssuer",
        Audience = "PorQueFaltaAudience",
        SigningCredentials = assinatura,
        Subject = identity,
        NotBefore = DateTime.UtcNow,
        Expires = DateTime.UtcNow.AddHours(12)
      };

      SecurityToken token = handler.CreateToken(tokenDescriptor);
      return handler.WriteToken(token);
    }

    /// <summary>
    /// Criptogra a senha do usuário
    /// </summary>
    /// <param name="plainText">Senha pura</param>
    /// <param name="password">Entra de criptografia</param>
    /// <returns>Senha encriptada</returns>
    public static string Encrypt(string plainText, string password = key)
    {
      if (plainText == null)
        return null;

      if (password == null)
        password = string.Empty;

      byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(plainText);
      byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

      using (SHA256 sha256Hash = SHA256.Create())
        passwordBytes = sha256Hash.ComputeHash(passwordBytes);

      byte[] bytesEncrypted = Encrypt(bytesToBeEncrypted, passwordBytes);
      return Convert.ToBase64String(bytesEncrypted);
    }

    /// <summary>
    /// Descriptografa senha do usuário
    /// </summary>
    /// <param name="encryptedText">Senha encriptada</param>
    /// <param name="password">Entra de descriptografia</param>
    /// <returns>Senha pura</returns>
    public static string Decrypt(string encryptedText, string password = key)
    {
      if (encryptedText == null)
        return null;

      if (password == null)
        password = string.Empty;

      byte[] bytesToBeDecrypted = Convert.FromBase64String(encryptedText);
      byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

      using (SHA256 sha256Hash = SHA256.Create())
        passwordBytes = sha256Hash.ComputeHash(passwordBytes);

      byte[] bytesDecrypted = Decrypt(bytesToBeDecrypted, passwordBytes);

      return Encoding.UTF8.GetString(bytesDecrypted);
    }

    /// <summary>
  /// Monta cifragem da senha
  /// </summary>
  /// <param name="bytesToBeEncrypted">Bytes para encryptação</param>
  /// <param name="passwordBytes">Bytes de senha</param>
  /// <returns>array de bytes de encryptação</returns>
  private static byte[] Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
  {
    byte[] encryptedBytes = null;
    byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

    using (MemoryStream ms = new MemoryStream())
    {
      using RijndaelManaged AES = new RijndaelManaged();
      using Rfc2898DeriveBytes KeyId = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000, HashAlgorithmName.SHA512);

      AES.KeySize = 256;
      AES.BlockSize = 128;
      AES.Key = KeyId.GetBytes(AES.KeySize / 8);
      AES.IV = KeyId.GetBytes(AES.BlockSize / 8);

      AES.Mode = CipherMode.CBC;

      using (CryptoStream cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
      {
        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
        cs.Close();
      }

      encryptedBytes = ms.ToArray();
    }
    return encryptedBytes;
  }

  /// <summary>
  /// Desmonta cifragem da senha
  /// </summary>
  /// <param name="bytesToBeDecrypted">Bytes encriptado</param>
  /// <param name="passwordBytes">Bytes da senha de descriptografia</param>
  /// <returns>Bytes descriptado</returns>
  private static byte[] Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
  {
    byte[] decryptedBytes = null;

    byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

    using (MemoryStream ms = new MemoryStream())
    {
      using RijndaelManaged AES = new RijndaelManaged();
      using (Rfc2898DeriveBytes KeyId = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000, HashAlgorithmName.SHA512))
      {
        AES.KeySize = 256;
        AES.BlockSize = 128;
        AES.Key = KeyId.GetBytes(AES.KeySize / 8);
        AES.IV = KeyId.GetBytes(AES.BlockSize / 8);
        AES.Mode = CipherMode.CBC;
      }

      using (CryptoStream cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
      {
        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
        cs.Close();
      }

      decryptedBytes = ms.ToArray();
    }

    return decryptedBytes;
  }

  /// <summary>
  /// Salva log de autenticação e evento de download de arquivos
  /// </summary>
  /// <param name="events">Eventos á serem salvos</param>
  /// <param name="context">Contexto do banco de dados</param>
  public static void SaveLogEvent(MO.LogEvents events)
  {
    using DataContext _dbContext = new DataContext();
    _dbContext.LogEventos.Add(events);
    _dbContext.SaveChanges();
  }

  /// <summary>
  /// Obtém o nome da classe e execução no formato async
  /// </summary>
  /// <param name="name">Name Class Async</param>
  /// <returns>Retorna o nome da classe</returns>
  public static string GetActualAsyncMethodName([System.Runtime.CompilerServices.CallerMemberName] string name = null) => name;

  }
}