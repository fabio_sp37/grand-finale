﻿using Microsoft.EntityFrameworkCore;
using porquefalta.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace porquefalta.BSS
{
    public class Profile
    {
        public async Task<List<MO.Profile>> GetProfiles()
        {
            using DataContext context = new DataContext();
            var t = await context.Perfil.ToListAsync();

            return t;
        }
    }
}
